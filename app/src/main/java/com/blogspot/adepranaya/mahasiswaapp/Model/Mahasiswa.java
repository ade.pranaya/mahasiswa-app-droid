 package com.blogspot.adepranaya.mahasiswaapp.Model;

//import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Mahasiswa {

    @SerializedName("id")
//    @Expose
    private String id;
    @SerializedName("nama")
//    @Expose
    private String nama;
    @SerializedName("nrp")
//    @Expose
    private String nrp;
    @SerializedName("email")
//    @Expose
    private String email;
    @SerializedName("jurusan")
//    @Expose
    private String jurusan;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNrp() {
        return nrp;
    }

    public void setNrp(String nrp) {
        this.nrp = nrp;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getJurusan() {
        return jurusan;
    }

    public void setJurusan(String jurusan) {
        this.jurusan = jurusan;
    }

}