package com.blogspot.adepranaya.mahasiswaapp.Model;
import com.google.gson.annotations.SerializedName;
import java.util.List;

public class GetMahasiswa {
    @SerializedName("mahasiswa")
    private List<Mahasiswa> mahasiswa = null;

    public List<Mahasiswa> getMahasiswa() {
        return mahasiswa;
    }

    public void setMahasiswa(List<Mahasiswa> mahasiswa) {
        this.mahasiswa = mahasiswa;
    }
}


