package com.blogspot.adepranaya.mahasiswaapp.Adapter;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

//import com.example.root.kontak.EditActivity;
//import com.example.root.kontak.Model.Kontak;
//import com.example.root.kontak.R;

import com.blogspot.adepranaya.mahasiswaapp.Model.Mahasiswa;
import com.blogspot.adepranaya.mahasiswaapp.R;

import java.util.List;

/**
 * Created by root on 2/3/17.
 */

public class MahasiswaAdapter extends RecyclerView.Adapter<MahasiswaAdapter.MyViewHolder> {
    List<Mahasiswa> mMahasiswaList;

    public MahasiswaAdapter(List<Mahasiswa> MahasiswaList) {
        mMahasiswaList = MahasiswaList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_mahasiswa, parent, false);
        MyViewHolder mViewHolder = new MyViewHolder(mView);
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.mTextViewId.setText("Id = " + mMahasiswaList.get(position).getId());
        holder.mTextViewNama.setText(mMahasiswaList.get(position).getNama().toString());
        holder.mTextViewNrp.setText("Nrp = " + mMahasiswaList.get(position).getNrp());
        holder.mTextViewEmail.setText("Email = " + mMahasiswaList.get(position).getEmail());
        holder.mTextViewJurusan.setText("Jurusan = " + mMahasiswaList.get(position).getJurusan());
        holder.editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(view.getContext(),"edit button clicked", Toast.LENGTH_SHORT).show();
            }
        });
        holder.deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(view.getContext(),"delete button clicked", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mMahasiswaList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView mTextViewId, mTextViewNama, mTextViewNrp, mTextViewEmail, mTextViewJurusan;
         Button editBtn, deleteBtn;

        public MyViewHolder(View itemView) {
            super(itemView);
            mTextViewId = itemView.findViewById(R.id.idTxt);
            mTextViewNama = itemView.findViewById(R.id.namaTxt);
            mTextViewNrp = itemView.findViewById(R.id.nrpTxt);
            mTextViewEmail = itemView.findViewById(R.id.emailTxt);
            mTextViewJurusan = itemView.findViewById(R.id.jurusanTxt);
            editBtn = itemView.findViewById(R.id.editBtn);
            deleteBtn = itemView.findViewById(R.id.deleteBtn);
        }
    }
}
