package com.blogspot.adepranaya.mahasiswaapp.Rest;

import com.blogspot.adepranaya.mahasiswaapp.Model.GetMahasiswa;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ApiInterface {
    @GET("mahasiswa")
    Call<GetMahasiswa> getMahasiswa();
//    Call<List<Repo>> listRepos(@Path("user") String user);
//    @FormUrlEncoded
//    @POST("kontak")
//    Call postKontak(@Field("nama") String nama,
//                    @Field("nomor") String nomor);
//    @FormUrlEncoded
//    @PUT("kontak")
//    Call putKontak(@Field("id") String id,
//                   @Field("nama") String nama,
//                   @Field("nomor") String nomor);
//    @FormUrlEncoded
//    @HTTP(method = "DELETE", path = "kontak", hasBody = true)
//    Call deleteKontak(@Field("id") String id);
}
