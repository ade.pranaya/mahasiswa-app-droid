package com.blogspot.adepranaya.mahasiswaapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.blogspot.adepranaya.mahasiswaapp.Adapter.MahasiswaAdapter;
import com.blogspot.adepranaya.mahasiswaapp.Model.GetMahasiswa;
import com.blogspot.adepranaya.mahasiswaapp.Model.Mahasiswa;
import com.blogspot.adepranaya.mahasiswaapp.Rest.ApiClient;
import com.blogspot.adepranaya.mahasiswaapp.Rest.ApiInterface;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    Button btIns;
    ApiInterface mApiInterface;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter<MahasiswaAdapter.MyViewHolder> mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    public static MainActivity ma;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mApiInterface = ApiClient.getClient().create(ApiInterface.class);
        ma = this;
        refresh();
    }

    public void refresh() {
        Call<GetMahasiswa> mahasiswaCall = mApiInterface.getMahasiswa();
        mahasiswaCall.enqueue(new Callback<GetMahasiswa>() {
            @Override
            public void onResponse(Call<GetMahasiswa> call, Response<GetMahasiswa>
                    response) {
                List<Mahasiswa> mahasiswaList = response.body().getMahasiswa();
                System.out.println(mahasiswaList.toArray().toString());
                Log.d("Retrofit Get", "Jumlah data Mahasiswa: " +
                        String.valueOf(mahasiswaList.size()));
                mAdapter = new MahasiswaAdapter(mahasiswaList);
                mRecyclerView.setAdapter(mAdapter);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Log.e("Retrofit Get", t.toString());
            }
        });
    }
}
